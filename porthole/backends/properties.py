#!/usr/bin/env python3

"""
    Porthole Properties class
    Holds all properties from an ebuild

    Copyright (C) 2003 - 2008 Fredrik Arnerup, Daniel G. Taylor
    Brian Dolbec, Wm. F. Wheeler, Tommy Iorns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import datetime
import logging

logger = logging.getLogger(__name__)
logger.debug("PROPERTIES: id initialized to %d", datetime.datetime.now().microsecond)

# Cherry pick from https://github.com/fcolecumberri/porthole/commit/f2f6269ee5a0a8b4bde3768904ce0d70cddbee17
def comp_iuse(iuse):
    if iuse:
        if len(iuse) > 1:
            if (iuse[0] == '+') or (iuse[0] == '-'):
                return iuse[1:]
    return iuse



class Properties:
    """Contains all variables in an ebuild."""

    def __init__(self, dictionary=None):
        self.__dict = dictionary
        # logger.debug("PORTAGELIB: INIT Properties= %s", dictionary)

    def __getattr__(self, name):
        try:
            return self.__dict[name]
        except ValueError:
            return ''

    def get_slot(self):
        """Return ebuild slot"""
        return self.slot

    def get_keywords(self):
        """Returns a list of strings."""
        return self.keywords.split()

    def get_use_flags(self):
        # Cherry pick from https://github.com/fcolecumberri/porthole/commit/f2f6269ee5a0a8b4bde3768904ce0d70cddbee17
        """Returns a list of strings."""
        ## iuse may have dupes, so filter them out
        iuse_flags = list(set(self.iuse.split()))
        iuse_flags.sort(key=comp_iuse)
        return iuse_flags
        # return list(set(self.iuse.split()))
    def get_homepages(self):
        """Returns a list of strings."""
        return self.homepage.split()
