#!/usr/bin/env python3

"""
    Backends Utilities
    helper functions for the portage libraries and/or porthole

    Fixed for Python 3 - April 2020 Michael Greene

    Copyright (C) 2003 - 2008 Fredrik Arnerup, Daniel G. Taylor,
    Wm. F. Wheeler, Brian Dolbec, Tommy Iorns

    Copyright: 2005 Brian Harring <ferringb@gmail.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import datetime
import logging
from gettext import gettext as _
from porthole.utils import debug
from porthole.backends import portagelib as portage_lib

logger = logging.getLogger(__name__)
logger.debug("UTILITIES: id initialized to %d", datetime.datetime.now().microsecond)

# circular import problem
# from porthole.db import userconfigs
USERCONFIGS = None

"""
The following strips blank lines and comments from the input file and returns
the result in a list
"""


# And now for some code stolen from pkgcore :)
# Copyright: 2005 Brian Harring <ferringb@gmail.com>
# License: GPL2
def iter_read_bash(bash_source):
    """read file honoring bash commenting rules.  Note that it's considered good behaviour to close
        filehandles, as such, either iterate fully through this, or use read_bash instead.
        once the file object is no longer referenced, the handle will be closed, but be proactive
        instead of relying on the garbage collector."""
    try:
        if isinstance(bash_source, str):
            bash_source = open(bash_source, 'r')
        for s in bash_source:
            s = s.strip()
            if s.startswith("#") or s == "":
                continue
            yield s
        bash_source.close()
    except IOError:
        pass


def read_bash(bash_source):
    logger.debug("BACKENDS Utilities: read_bash file: %s", bash_source)
    return list(iter_read_bash(bash_source))


# end of stolen code


def sort(sortlist):
    """sort in alphabetic instead of ASCIIbetic order"""
    logger.debug("BACKENDS Utilities: sort()")
    try:
        spam = [(x[0].upper(), x) for x in sortlist]
        spam.sort()
        # debug.dprint("BACKENDS Utilities: sort(); finished")
        return [x[1] for x in spam]
    except ValueError:
        logger.debug(
            "BACKENDS Utilities: sort(); failed for some reason, probably an index error so I'll print the list too: ")
        debug.dprint("BACKENDS Utilities: sort(); list = %s", str(list))
        return [_('None')]  # so it doesn't crash


def get_sync_info():
    """gets and returns the timestamp info saved during
        the last portage tree sync, example:
        last_sync, valid_sync  -->  Wed 22 Apr 2020 09:08:52 AM UTC, True
    """
    logger.debug("BACKENDS Utilities: get_sync_info();")
    last_sync = _("Unknown") + '  '  # need a space at end of string cause it will get trimmed later
    valid_sync = False
    try:
        logger.debug("BACKENDS Utilities: portdir: %s", portage_lib.settings.portdir)
        f = open(portage_lib.settings.portdir + "/metadata/timestamp")
        data = f.read()
        f.close()
        # there was code for py2.7 and unicode conversion, this is a nonsense kind of
        # check in place
        if isinstance(data, str):
            last_sync = data.strip()
            valid_sync = True
        else:
            logger.debug("BACKENDS Utilities: get_sync_info(); No data read")
            valid_sync = False
    # except os.error:
    except IOError as e:
        errno, strerror = e.args
        logger.debug("BACKENDS Utilities: get_sync_info(); I/O Error: %s  (%d)", str(strerror), errno)
    logger.debug("BACKENDS Utilities: Timestamp: %s  Valid %s", last_sync, valid_sync)
    logger.debug("BACKENDS Utilities: get_sync_info(); *** done")
    return last_sync, valid_sync


def reduce_flags(flags):
    """function to reduce a list of 'USE' flags to their final setting"""
    # debug.dprint("BACKENDS Utilities: reduce_flags(); flags = %s" %str(flags))
    myflags = []
    for x in flags:

        if x[0] == "+":
            # debug.dprint("BACKENDS Utilities: USE flags should not start with a '+': " + x)
            x = x[1:]
            if not x:
                continue

        if x[0] == "-":
            try:
                myflags.remove(x[1:])
            except ValueError:
                logger.debug("BACKENDS Utilities: reduce_flags(); x[0] == '-', flag %s not found", str(x[1:]))
                myflags.append(x)
            # continue

        if x not in myflags:
            myflags.append(x)

    return myflags


def flag_defaults(the_list):
    defaults = []
    for flag in the_list:
        if flag[0] in ["+", "-"]:
            if flag[0] == "+":
                flag = flag[1:]
            defaults.append(flag)
    return defaults


def get_reduced_flags(ebuild):
    """function to get all use flags for an ebuild or package and reduce them to their final setting"""
    global USERCONFIGS
    if USERCONFIGS is None:  # avaoid a circular import problem
        # noinspection PyProtectedMember
        from porthole.db import userconfigs
        USERCONFIGS = userconfigs
    # ~ props = portage_lib.get_properties(ebuild) ~ IUSE_defaults = flag_defaults(props.get_use_flags()) ~
    # debug.dprint("BACKENDS Utilities: get_reduced_flags(); IUSE_defaults = %s" %str(IUSE_defaults)) ~ debug.dprint(
    # "BACKENDS Utilities: get_reduced_flags(); SystemUseFlags = " + str(portage_lib.settings.SystemUseFlags)) ~ del
    # props Check package.use to see if it applies to this ebuild at all
    package_use_flags = USERCONFIGS.get_user_config('USE', ebuild=ebuild)
    # debug.dprint("BACKENDS Utilities: get_reduced_flags(); package_use_flags = %s" %str(package_use_flags))
    if package_use_flags is not None and package_use_flags is not []:
        # debug.dprint("BACKENDS Utilities: get_reduced_flags(); adding package_use_flags to ebuild_use_flags")
        # debug.dprint("BACKENDS Utilities: get_reduced_flags(); SystemUseFlags = " + str(
        # portage_lib.settings.SystemUseFlags)) ebuild_use_flags = reduce_flags(IUSE_defaults +
        # portage_lib.settings.SystemUseFlags + package_use_flags)
        ebuild_use_flags = reduce_flags(portage_lib.settings.SystemUseFlags + package_use_flags)
    else:
        # debug.dprint("BACKENDS Utilities: get_reduced_flags(); adding only system_use_flags to ebuild_use_flags")
        # ebuild_use_flags = reduce_flags(IUSE_defaults + portage_lib.settings.SystemUseFlags)
        ebuild_use_flags = reduce_flags(portage_lib.settings.SystemUseFlags)
    # debug.dprint("BACKENDS Utilities: get_reduced_flags(); final ebuild_use_flags = %s" %str(ebuild_use_flags))
    return ebuild_use_flags


def abs_flag(flag):
    if flag[0] in ["+", "-"]:
        return flag[1:]
    else:
        return flag


def abs_list(the_list):
    r = []
    for member in the_list:
        r.append(abs_flag(member))
    return r


def slot_split(mydep):
    colon = mydep.rfind(":")
    if colon != -1:
        return [mydep[:colon], mydep[colon + 1:]]
    return mydep, ''


def use_required_split(mydep):
    brace = mydep.find("[")
    if brace != -1:
        brace2 = mydep.rfind("]")
        return [mydep[:brace], mydep[brace + 1:brace2]]
    return mydep, ''


comparators = ["~", "<", ">", "=", "<=", ">="]


def comparators_split(mydep):
    cmp = ""
    if mydep.endswith('*'):
        cmp = "=*"
        mydep = mydep[1:-1]
    else:
        for c in comparators:
            if mydep.startswith(c):
                cmp = c
        mydep = mydep[len(cmp):]
    return mydep, cmp


def dep_split(mydep):
    if mydep == '':
        return '', '', '', ''
    mydep, use = use_required_split(mydep)
    mydep, slot = slot_split(mydep)
    mydep, cmp = comparators_split(mydep)
    return mydep, cmp, slot, use


def filter_flags(myuse, use_expand_hidden, usemasked, useforced):
    # clean out some environment flags, since they will most probably be confusing for the user
    for f in use_expand_hidden:
        f = f.lower() + "_"
        for x in myuse:
            if f in x:
                myuse.remove(x)
    # clean out any arch's
    archlist = portage_lib.get_archlist()
    for a in myuse[:]:
        if a in archlist:
            myuse.remove(a)
    # dbl check if any from usemasked  or useforced are still there
    masked = usemasked + useforced
    for a in myuse[:]:
        if a in masked:
            logger.debug("BACKENDS Utilities:  filter_flags(); found %s in usemask... removing", str(a))
            myuse.remove(a)
    logger.debug("BACKENDS Utilities:  filter_flags(); filtered myuse = %s", str(myuse))
    return myuse
