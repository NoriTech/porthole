#!/usr/bin/env python3

"""
    Porthole Database
    Holds all portage packages and functions

    Fixed for Python 3 and modified - April/August 2020 Michael Greene

    Copyright (C) 2003 - 2008 Fredrik Arnerup, Daniel G. Taylor
    Brian Dolbec, Wm. F. Wheeler, Tommy Iorns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

# Package modules
__all__ = ['database', 'dbbase', 'dbreader', 'package', 'user_configs']

import datetime
import logging
from porthole.db.database import Database
from porthole.db.user_configs import UserConfigs

logger = logging.getLogger(__name__)
logger.debug("DB-DB Package: import id initialized to %d", datetime.datetime.now().microsecond)

# This kicks off the Database thread to load databases.
logger.debug("DATABASE: Database() initalized as db")
db = Database()

logger.debug("DATABASE: UserConfigs() initalized as userconfigs")
userconfigs = UserConfigs(True)
