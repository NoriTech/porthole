#!/usr/bin/env python3

"""
    Package Database structure

    Fixed for Python 3 - April 2020 Michael Greene

    Copyright (C) 2003 - 2008 Fredrik Arnerup, Daniel G. Taylor,
    Wm. F. Wheeler, Brian Dolbec, Tommy Iorns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import datetime
import logging

logger = logging.getLogger(__name__)
logger.debug("DB-DBASE: id initialized to %d", datetime.datetime.now().microsecond)


class DBBase(object):
    def __init__(self):
        # category dictionary with sorted lists of all ebuilds/packages
        #  db.categories
        #       category
        #           package -- Package Object
        #           package -- Package Object
        #       category
        #           package -- Package Object
        #           package -- Package Object
        # ... and so on.
        self.categories = {}
        # all packages in a directory sorted by package name
        #  db.list
        #       package -- Package Object
        #       package -- Package Object
        # ... and so on.
        self.list = []
        #
        # As note -
        # categories point to package names which point to Package objects
        # list is an index of packages sorted names which points to the same
        # Package objects. I did change list to a dictionary
        #
        # category dictionary with sorted lists of installed packages
        #  db.installed
        #       category
        #           package -- Package Object
        #           package -- Package Object
        #       category
        #           package -- Package Object
        #           package -- Package Object
        # ... and so on.
        self.installed = {}
        #
        # keep track of the number of installed packages
        self.installed_count = 0
        # the next 2 dictionaries hold pkg counts for each category
        self.pkg_count = {}
        self.installed_pkg_count = {}

        self.deprecated_list = []

        self.world = []

        #
        # Package Object:
        #   full_name
        #   latest_ebuild
        #   hard_masked
        #   hard_masked_nocheck
        #   best_ebuild
        #   installed_ebuilds
        #   name
        #   category
        #   properties = {}
        #   upgradable
        #   dep_upgradable
        #   latest_installed
        #   size
        #   digest_file
        #   in_world        True / False
        #   is_checked      True / False
        #   deprecated      True / False
        #
