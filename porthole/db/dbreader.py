#!/usr/bin/env python3

"""
    Dbreader, class for reading the portage tree and building a porthole database

    Fixed for Python 3 - April 2020 Michael Greene

    Copyright (C) 2003 - 2009 Fredrik Arnerup, Daniel G. Taylor,
    Wm. F. Wheeler, Brian Dolbec, Tommy Iorns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import os
import threading
import datetime
import logging
from collections import OrderedDict
from porthole.db.package import Package
from porthole.db.dbbase import DBBase
from porthole.backends import portagelib as portage_lib

logger = logging.getLogger(__name__)
logger.debug("DB-DBREADER: import id initialized to %d", datetime.datetime.now().microsecond)


class DatabaseReader(threading.Thread):
    """Builds the database in a separate thread."""

    def __init__(self, callback):
        threading.Thread.__init__(self)
        self.id = datetime.datetime.now().microsecond
        logger.debug("DBREADER: DatabaseReader.id initialized to %d", self.id)
        self.daemon = True  # quit even if this thread is still running
        self.db = DBBase()  # the database:
        self.callback = callback  # callback for statusbar and running status
        self.done = False  # false if the thread is still working
        self.nodecount = 0  # number of nodes read so far
        self.error = ""  # may contain error message after completion
        self.done = False  # database status flag, when complete then True
        self.cancelled = False  # cancelled will be set when the thread should stop
        self.allnodes = {}  # all ebuilds
        self.allnodes_length = 0  # used for calculating the progress bar
        self.world = portage_lib.settings.get_world()  # defined in DBBASE value set here - is this used????
        self.installed_list = []  # installed packages
        self.installed_count = 0  # number of installed packages

    def please_die(self):
        """ Tell the thread to die """
        self.cancelled = True

    def thread_stop_check(self):
        """*** Thread cancelled check """
        if self.cancelled:  # cancelled will be set when the thread should stop
            self.done = True  # stop thread - return from read_db(self)
        return

    def set_db_callback(self, node, length, status, error):
        """ setup callback to update statusbar and run status """
        self.callback({"nodecount": node,
                       "allnodes_length": length,
                       "done": status,
                       'db_thread_error': error
                       })
        return

    def get_db(self):
        """Returns the database that was read."""
        return self.db

    def read_db(self):
        """Read portage's database and store it nicely"""
        logger.debug("DBREADER: read_db(); process id = %d *****************", os.getpid())

        # vartree - installed ebuilds - self.installed_list and self.installed_count
        self.installed_list = portage_lib.get_vartree()
        self.installed_count = len(self.installed_list)
        logger.debug("DBREADER: read_db(); getting installed package list, num %d", self.installed_count)

        # porttree - all ebuilds - self.allnodes and self.allnodes_length
        self.allnodes = portage_lib.get_porttree()
        self.allnodes_length = len(self.allnodes)
        logger.debug("DBREADER: read_db(); getting allnodes package list, num %d", self.allnodes_length)

        logger.debug("DBREADER: read_db(); Threading info: %s  %d", threading.get_ident(), threading.active_count())
        #
        # Process all ebuild list
        #
        count = 0
        logger.debug("DBREADER: Processing allnodes")
        for entry in self.allnodes:
            self.thread_stop_check()

            if count == 250:  # update the statusbar
                self.nodecount += count
                self.set_db_callback(self.nodecount, self.allnodes_length, self.done, self.error)
                count = 0
            # pass  category/package to add_pkg
            count += self.add_pkg(entry)
        logger.debug("DBREADER: Processed allnodes %d", count)
        #
        # now time to add any remaining installed packages not in the portage tree
        #
        logger.debug("DBREADER: Processing deprecated_list")
        # during add_pkg above, entries are removed from installed_list as they are added to
        # db.installed. So at this point, installed_list only contains entries that are not
        # present in allnodes (all ebuilds) or, better said, it is a list of deprecated entries.
        # The original code sets db.deprecated_list in the database equal to this list and
        # installed_list used again in "if" below
        self.db.deprecated_list = self.installed_list[:]
        # run through the list of deprecated packages
        for entry in self.db.deprecated_list:  # remaining installed packages no longer in the tree
            self.thread_stop_check()

            if count == 250:  # update the statusbar -- really? This many deprecated packages?
                self.nodecount += count
                self.set_db_callback(self.nodecount, self.allnodes_length, self.done, self.error)
                count = 0
            logger.debug("DBREADER: read_db(); deprecated entry = %s", entry)
            # same Package add as above with deprecated=True
            count += self.add_pkg(entry, deprecated=True)

        logger.debug("DBREADER: read_db(); end of list build; count = %d nodecount = %d", count, self.nodecount)
        self.nodecount += count
        logger.debug("DBREADER: read_db(); end of list build; final nodecount = %d categories = %d sort is next",
                     self.nodecount, len(self.db.categories))
        # self.db.list = self.sort(self.db.list)
        # self.db.list = OrderedDict(sorted(self.db.list.items()))
        logger.debug("DBREADER: read_db(); end of sort, finished")

    def add_pkg(self, entry, deprecated=False):
        # logger.debug("DBREADER: add_pkg(); entry = %s", entry)
        self.thread_stop_check()

        category, name = entry.split('/')

        # main point - make package entry for all ebuilds
        # runs through all ebuilds and final form is:
        #
        #  db.categories
        #       category
        #           pacckage -- Package Object
        #           pacckage -- Package Object
        #       category
        #           pacckage -- Package Object
        #           pacckage -- Package Object
        # ... and so on.
        #
        # during initial setup deprecated set to False
        # data = Package(category, name)
        data = Package(entry)
        data.deprecated = deprecated  # default false unless True passed

        # note: on second round processing adds deprecated to all ebuilds and installed
        # data variables - not sure why both at this time

        # build category list in database
        if category not in self.db.categories:
            self.db.categories[category] = {}
            self.db.pkg_count[category] = 0
            logger.debug("DBREADER: add_pkg(); added category %s", str(category))
        self.db.categories[category][name] = data
        #
        # installed_list contains all installed packages
        # if entry (allnodes) in installed_list setup tree as above:
        #
        #  db.installed
        #       category
        #           pacckage -- Package Object
        #           pacckage -- Package Object
        #       category
        #           pacckage -- Package Object
        #           pacckage -- Package Object
        # ... and so on.
        #
        if entry in self.installed_list:
            # build category list in database
            if category not in self.db.installed:
                self.db.installed[category] = {}
                self.db.installed_pkg_count[category] = 0
                logger.debug("DBREADER: add_pkg(); added installed category %s", str(category))
            self.db.installed[category][name] = data
            self.db.installed_pkg_count[category] += 1
            self.db.installed_count += 1
            # remove entry from installed list since it has been added to the db
            self.installed_list.remove(entry)
        # self.db.list[name] = data  # changed to dict from self.db.list.append((name, data))
        self.db.list.append((name, data))
        self.db.pkg_count[category] += 1
        return 1  # return 1 to increment count

    def get_installed(self):
        """get a new installed list"""
        # get list from portagelib -- vartree every installed ebuild
        logger.debug("DBREADER: get_installed();")
        self.installed_list = portage_lib.get_installed_list()
        self.installed_count = len(self.installed_list)

    def run(self):
        """The thread function."""
        logger.debug("DBREADER: DatabaseReader.run(); start")
        self.read_db()
        self.done = True  # tell main thread that this thread has finished and pass back the db
        self.set_db_callback(self.nodecount, self.allnodes_length, self.done, self.error)
        logger.debug("DBREADER: DatabaseReader.run(); finished")
