#!/usr/bin/env python3

"""
    Porthole Configuration Dialog
    Allows the user to Configure Porthole

    Fixed for Python 3 - May 2020 Michael Greene

    Copyright (C) 2005 - 2008 Brian Dolbec, Thomas Iorns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    **************************************************************************

    Correcting the glade files where already a big PITA, but specifically the
    config.glade. I start correcting the GUI and then as I go through the code
    I find most of the options are not implemented!!!! See self.hide_widgets()!

    WTF - since the Porthole is so old, I opt for a stable working version, so
    all the "not implemented" stuff removed. Someone else wants to put it back
    in - have a ball!

    Some of the issues I considered - mainly there are too many options. I can
    understand maybe wanting to change some colors, but there are too many options.
    Second, I think it should use system font and not be selectable. Granted,
    Porthole is what, 15 years old? It still has that old Gnome-ish feel in
    configurations.

    1. self.hide_widgets() Hide widgets which haven't been implemented yet.
    Removed those widgets and call.
    2. The color options are overwhelming, need to simplify.
"""

import datetime
import logging

logger = logging.getLogger(__name__)
logger.debug("CONFIGURE: id initialized to %d", datetime.datetime.now().microsecond)

import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk
from gi.repository import Gdk

import webbrowser

from porthole import backends
portage_lib = backends.portage_lib
from porthole.config import Config
from porthole.config import Paths


class ConfigDialog:
    """Class to display a GUI for configuring Porthole"""

    def __init__(self):
        """ Initialize Config GUI """

        # parse config.glade
        self._DATA_PATH = Paths.get_data_path()
        self.gladefile = self._DATA_PATH + '/glade/config.glade'
        self.builder = Gtk.Builder()
        self.builder.add_from_file(self.gladefile)

        # register callbacks
        callbacks = {
            "on_config_response": self.on_config_response,
            "on_color_set": self.on_color_set,
            "on_color_clicked": self.on_color_clicked,
            "on_enable_archlist_toggled": self.toggle_archlist,
            "on_globals_use_custom_browser_toggled": self.toggle_browser_table,
        }

        self.window = self.builder.get_object("config")  # overall config dialog
        self.KeywordsFrame = self.builder.get_object("archlist_frame")  # current arch profile
        self.builder.connect_signals(callbacks)

        # Build lists of widgets and equivalent prefs
        self.xtermtaglist = ['red', 'green', 'yellow', 'blue', 'magenta', 'cyan', 'white', 'black']

        # 'use_minus_fg', 'use_plus_fg' are in config.glade, but were not
        # listed here
        self.viewoptions = ['downgradable_fg', 'upgradable_fg',
                            'normal_fg', 'normal_bg',
                            'use_minus_fg', 'use_plus_fg'  # added
                            ]
        self.checkboxes = [
            ['globals', 'enable_archlist'],
            ['summary', 'showtable'],
            ['summary', 'showinstalled'],
            ['summary', 'showavailable'],
            ['summary', 'showkeywords'],
            ['summary', 'showurl'],
            ['summary', 'showuseflags'],
            ['summary', 'showlongdesc'],
            ['summary', 'showlicense'],
            ['emerge', 'pretend'],
            ['emerge', 'verbose'],
            ['emerge', 'nospinner'],
            ['emerge', 'fetch'],
            ['emerge', 'oneshot'],
            ['emerge', 'update'],
            ['emerge', 'noreplace'],
            ['advemerge', 'show_make_conf_button'],
        ]

        default_fg = Gtk.TextView().get_style_context().get_color(Gtk.StateFlags.NORMAL)

        # [['emerge --sync', 'Sync'], ['emerge-webrsync', 'WebRsync']]
        self.syncmethods = Config.Prefs.globals.Sync_methods

        # Display current CPU architecture
        widget = self.builder.get_object('current_arch')
        widget.set_label(Config.Prefs.myarch)

        # Checkboxes:
        for category, name in self.checkboxes:
            widget = self.builder.get_object('_'.join([category, name]))
            logger.debug("CONFIGDIALOG: set_widget_values(); Checkboxes: widget = %s", ('_'.join([category, name])))
            if widget:
                active = getattr(getattr(Config.Prefs, category), name)
                logger.debug("CONFIGDIALOG: cb active %s", str(active))
                if not active:
                    active = False
                widget.set_active(active)
            else:
                logger.debug("CONFIGDIALOG: set_widget_values(); Checkboxes: widget = %s not found!", (
                    '_'.join([category, name])))

        # Sync combobox
        store = Gtk.ListStore(str)
        widget = self.builder.get_object('sync_combobox')
        tempiter = None
        if widget:
            for command, label in self.syncmethods:
                if not command.startswith('#'):
                    tempiter = store.append([command])
                    if command == Config.Prefs.globals.Sync:
                        olditer = tempiter
            widget.set_model(store)
            cell = Gtk.CellRendererText()
            widget.pack_start(cell, True)
            widget.add_attribute(cell, 'text', 0)
            if tempiter:
                widget.set_active_iter(olditer)

        # Custom Command History
        for x in [1, 2, 3, 4, 5]:
            widget = self.builder.get_object('history' + str(x))
            if widget:
                pref = Config.Prefs.run_dialog.history[x]
                widget.set_text(pref)

        # custom browser command
        widget = self.builder.get_object('custom_browser_command')
        if widget:
            command = Config.Prefs.globals.custom_browser_command
            if command:
                widget.set_text(command)
            if not Config.Prefs.globals.use_custom_browser:
                self.builder.get_object('custom_browser_table').set_sensitive(False)

        # gui su client command
        widget = self.builder.get_object('su_client')
        if widget:
            command = Config.Prefs.globals.su
            if command:
                widget.set_text(command)

        # View Colours
        for name in self.viewoptions:
            color = getattr(Config.Prefs.views, name)
            widget = self.builder.get_object(name)
            if widget:
                if color:
                    widget.set_color(Gdk.color_parse(color))
                else:
                    pass
                    # widget.set_color(Gdk.color_parse(default_fg)) ToDo fix default.fg

        # Default XTerm colours:
        for name in self.xtermtaglist:
            color = Config.Prefs.TAG_DICT['fg_' + name][0]
            widget = self.builder.get_object('fg_' + name)
            if widget:
                if color:
                    widget.set_color(Gdk.color_parse(color))
                else:  # this should never happen, but just in case...
                    widget.set_color(Gdk.color_parse(name))
            color = Config.Prefs.TAG_DICT['bg_' + name][1]
            widget = self.builder.get_object('bg_' + name)
            if widget:
                if color:
                    widget.set_color(Gdk.color_parse(color))
                else:  # this should never happen, but just in case...
                    widget.set_color(Gdk.color_parse(name))


        # set widget values to values in prefs
        # self.set_widget_values()

    # -----------------------------------------------
    # GUI Callback function definitions start here
    # -----------------------------------------------

    def on_config_response(self, dialog_widget, response_id):
        """
        Parse dialog response (ok, cancel, apply or help clicked)
        Remember - glade uses numbers for responses -5, -6, -10, -11
        """
        logger.debug("CONFIGDIALOG: on_config_response(): response_id '%s'", response_id)
        if response_id == Gtk.ResponseType.OK:
            self.apply_widget_values()
            self.window.destroy()
        elif response_id == Gtk.ResponseType.CANCEL:
            self.window.destroy()
        elif response_id == Gtk.ResponseType.APPLY:
            self.apply_widget_values()
        elif response_id == Gtk.ResponseType.HELP:
            # Display help file with web browser
            webbrowser.open('file://' + Config.Prefs.DATA_PATH + 'help/customize.html')
        else:
            pass

    def on_color_clicked(self, button_widget, event):
        """ Make sure colour dialog doesn't show alpha choice """
        logger.debug("CONFIGDIALOG: on_color_clicked()")
        if event.button == 1:  # primary mouse button
            button_widget.set_use_alpha(False)
            return False  # continue to build colour selection dialog
        if event.button == 3:  # secondary mouse button
            if button_widget.get_alpha() > 40000:
                button_widget.set_alpha(32767)
                button_widget.set_use_alpha(True)
                name = button_widget.get_property('name')
                if name == 'default_fg':
                    button_widget.set_color(self.default_textview_fg)
                elif name == 'default_bg':
                    button_widget.set_color(self.default_textview_bg)
                else:
                    ext = name[-3:]
                    color = self.builder.get_object('default' + ext).get_color()
                    button_widget.set_color(color)
                self.on_color_set(button_widget)
                return True
            else:
                button_widget.set_alpha(65535)
                button_widget.set_use_alpha(False)
                self.on_color_set(button_widget)
                return True

    def on_color_set(self, button_widget):
        logger.debug("CONFIGDIALOG: on_color_set()")
        # if button is default-button: change colour in all buttons using default.
        color = button_widget.get_color()
        ext = None
        if button_widget.get_property('name') == 'default_fg':
            ext = "fg"
        elif button_widget.get_property('name') == 'default_bg':
            ext = "bg"
        if not ext or not hasattr(self, 'tagnamelist'):
            return
        for name in self.tagnamelist:
            widget = self.builder.get_object('_'.join([name, ext]))
            if widget:
                if widget.get_alpha() < 40000:
                    widget.set_color(color)

    # ------------------------------------------
    # Support function definitions start here
    # ------------------------------------------

    def set_widget_values(self):
        """ Set widget attributes based on prefs """

        # build the arch list widget
        self.build_archlist_widget()

    def apply_widget_values(self):
        """ Set prefs from widget values """

        # Default XTerm colours:
        for name in self.xtermtaglist:
            widget = self.builder.get_object('fg_' + name)
            if widget:
                color = widget.get_color()
                if color:
                    Config.Prefs.TAG_DICT['fg_' + name][0] = self.get_color_spec(color)
            widget = self.builder.get_object('bg_' + name)
            if widget:
                color = widget.get_color()
                if color:
                    Config.Prefs.TAG_DICT['bg_' + name][1] = self.get_color_spec(color)
                else:  # this should never happen, but just in case...
                    widget.set_color(Gtk.color_parse(name))

        # View Colours
        for name in self.viewoptions:
            widget = self.builder.get_object(name)
            if widget:
                color = widget.get_color()
                alpha = widget.get_alpha()
                if alpha:
                    setattr(Config.Prefs.views, name, self.get_color_spec(color))
                else:
                    setattr(Config.Prefs.views, name, '')

        # Checkboxes:
        for category, name in self.checkboxes:
            logger.debug("CONFIGDIALOG: apply_widget_values(); name = %s", name)
            widget = self.builder.get_object('_'.join([category, name]))
            if widget:
                # debug.dprint("CONFIGDIALOG: apply_widget_values(); name = %s widget found" %name)
                active = widget.get_active()
                setattr(getattr(Config.Prefs, category), name, active)
                if name == 'enable_archlist' and active:
                    archlist = []
                    keyword = ''
                    for item in self.kwList:
                        keyword = item[1]
                        if item[0].get_active():
                            logger.debug(item[1])
                            archlist.append(keyword)
                    logger.debug("CONFIGDIALOG: new archlist = %s", str(archlist))
                    Config.Prefs.globals.archlist = archlist[:]

        # Sync combobox
        widget = self.builder.get_object('sync_combobox')
        if widget:
            model = widget.get_model()
            iter = widget.get_active_iter()
            sync_command = model.get_value(iter, 0)
            for command, label in self.syncmethods:
                if command == sync_command:
                    Config.Prefs.globals.Sync = command
                    Config.Prefs.globals.Sync_label = label
                    break

        # Custom Command History
        for x in [1, 2, 3, 4, 5]:
            widget = self.builder.get_object('history' + str(x))
            if widget:
                text = widget.get_text()
                if text != Config.Prefs.run_dialog.history[x]:
                    Config.Prefs.run_dialog.history[x] = text

        # custom browser command
        widget = self.builder.get_object('custom_browser_command')
        if widget:
            text = widget.get_text()
            if text:
                Config.Prefs.globals.custom_browser_command = text

        # gui su client command
        widget = self.builder.get_object('su_client')
        if widget:
            text = widget.get_text()
            if text:
                Config.Prefs.globals.su = text

    def get_color_spec(self, color):
        red = hex(color.red)[2:].zfill(4)
        green = hex(color.green)[2:].zfill(4)
        blue = hex(color.blue)[2:].zfill(4)
        return '#' + red + green + blue

    def build_archlist_widget(self):
        """ Create a table layout and populate it with
            checkbox widgets representing the available
            keywords
        """
        logger.debug("CONFIGDIALOG: build_archlist_widget()")

        # If frame has any children, remove them
        child = self.KeywordsFrame.child
        if child != None:
            self.KeywordsFrame.remove(child)

        keywords = portage_lib.get_archlist()

        # Build table to hold radiobuttons
        size = len(keywords)
        maxcol = 3
        maxrow = size / maxcol - 1
        if maxrow < 1:
            maxrow = 1
        table = Gtk.Table(maxrow, maxcol - 1, True)
        self.KeywordsFrame.add(table)
        self.kwList = []

        # Iterate through use flags collection, create
        # checkboxes and attach to table
        col = 0
        row = 0
        button_added = False
        clickable_button = False
        for keyword in keywords:
            button = Gtk.CheckButton(keyword)
            self.kwList.append([button, keyword])
            table.attach(button, col, col + 1, row, row + 1)
            button.show()
            button_added = True
            clickable_button = True
            button.set_active(keyword in Config.Prefs.globals.archlist)
            # Increment col & row counters
            if button_added:
                col += 1
                if col > maxcol:
                    col = 0
                    row += 1
        if clickable_button:
            # Display the entire table
            table.show()
            self.KeywordsFrame.show()
            self.KeywordsFrame.set_sensitive(Config.Prefs.globals.enable_archlist)
        else:
            self.KeywordsFrame.set_sensitive(False)
        logger.debug("CONFIGDIALOG: build_archlist_widget(); widget build completed")

    def toggle_archlist(self, widget):
        """Toggles the archlist frame sensitivity
        """
        logger.debug("CONFIGDIALOG: toggle_archlist(); signal caught")
        self.KeywordsFrame.set_sensitive(widget.get_active())
        Config.Prefs.globals.enable_archlist = widget.get_active()

    def toggle_browser_table(self, widget):
        """Toggles custom browser command sensitivity
        """
        logger.debug("CONFIGDIALOG: toggle_browser_table()")
        self.builder.get_object('custom_browser_table').set_sensitive(widget.get_active())
