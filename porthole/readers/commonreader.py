#!/usr/bin/env python3

"""
    Porthole Reader Class: CommonReader

    Fixed for Python 3 - March 2020 Michael Greene

    Copyright (C) 2003 - 2008 Fredrik Arnerup, Brian Dolbec,
    Daniel G. Taylor and Wm. F. Wheeler, Tommy Iorns

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""

import threading


class CommonReader(threading.Thread):
    """ Common data reading class that works in a seperate thread """

    def __init__(self):
        """ Initialize """
        threading.Thread.__init__(self)
        self.daemon = True  # quit even if this thread is still running
        self.done = False  # false if the thread is still working
        self.cancelled = False  # cancelled will be set when the thread should stop
        self.count = 0  # for keeping status

        # print >>stderr,  "threading.enumerate() = ",threading.enumerate()
        # print >>stderr, "this thread is :", thread.get_ident(), ' current thread ', threading.currentThread()

    def please_die(self):
        """ Tell the thread to die """
        self.cancelled = True
