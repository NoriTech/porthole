import subprocess
import time
import threading
# from asyncio.subprocess import Process

class Command(object):
    '''
    Enables to run subprocess commands in a different thread
    '''
    def __init__(self, cmd):
        self.cmd = cmd
        self.process = None
        self.running = False
        print(self.cmd)

    def start(self, timeout=0, **kwargs):
        self.process = subprocess.run(self.cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        #thread = threading.Thread(target=target, kwargs=kwargs)
        #thread.start()
        #self.running = True
        #thread.join( )

        #while thread.is_alive():
        #    time.sleep(2)
        #    print("thread running")
        #else:
        #    print("thread stop")
        #    self.running = False

    def cmd_running(self):
        return self.running

        #    print("running")
        #    self.process.terminate()
        #    thread.join()

        #return True
